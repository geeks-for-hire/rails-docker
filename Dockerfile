FROM ruby:2.3-alpine

ENV PATH /root/.yarn/bin:$PATH

RUN apk add --no-cache \
    curl-dev \
    build-base \
    tzdata \
    libxml2 \
    libxml2-dev \
    libxslt \
    libxslt-dev \
    curl \
    bash \
    binutils \
    tar \
    git \
    nodejs \
    imagemagick-dev \
    postgresql-dev

RUN curl -o- -L https://yarnpkg.com/install.sh | bash


RUN apk del curl bash binutils tar
